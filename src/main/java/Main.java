package com.company;

public class Main {

    public static void main(String[] args) {
        User u1 = new User((long) 1, "admin", "pass");
        System.out.println(u1.toString());
        System.out.println();

        Cloth c1 = new Cloth((long) 2, "ciuch", 1.5, 300.772, "black", 23, "M", "Cotton");
        System.out.println(c1.getMaterial());
    }
}
